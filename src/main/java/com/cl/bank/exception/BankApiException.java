package com.cl.bank.exception;

import com.cl.bank.utils.MessageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BankApiException extends Exception {
    /**
     *
     */
    private static final long serialVersionUID = 8072531105668136904L;

    private Logger logger = LoggerFactory.getLogger(BankApiException.class);

    private BankApiError motif;
    private String code;
    private FieldErrorDTO fieldError;

    public FieldErrorDTO getFieldError() {
        return fieldError;
    }

    public void setFieldError(FieldErrorDTO fieldError) {
        this.fieldError = fieldError;
    }


    public BankApiException(Throwable cause) {
        super(cause);
        this.motif = BankApiError.ERR_API;
        logger.error(this.getMessage());
    }

    public BankApiException(String txt, BankApiError cause, String code) {
        super(txt);
        this.motif = cause;
        this.code = code;
        logger.error(this.getMessage());
    }

    @Override
    public String getMessage() {
        return super.getMessage();

    }

    public BankApiError getMotif() {
        if (motif == null) {
            motif = BankApiError.ERR_API;
        }
        return motif;
    }

    public void setMotif(BankApiError motif) {
        this.motif = motif;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


    public static BankApiException unprocessableEntityExceptionBuilder(String key, String[] objects) {

        String message = MessageFactory.getMessage("bank.api.service.unprocessable_entity_exception." + key, objects);
        return new BankApiException(message, BankApiError.UNPROCESSABLE_ENTITY, null);
    }

    public static BankApiException validationErrorBuilder(FieldErrorDTO fieldError) {

        String message = MessageFactory.getMessage("bank.api.exception.validation_error." + fieldError.getMessage(), new String[]{fieldError.getObjectName(), fieldError.getField()});
        BankApiException ex = new BankApiException(message, BankApiError.VALIDATION_ERROR, null);
        ex.setFieldError(fieldError);
        return ex;
    }


    public static BankApiException resourceNotFoundExceptionBuilder(String object, String reference) {

        return new BankApiException(MessageFactory.getMessage("bank.api.exception.resource_not_found",
                new String[]{object, reference}), BankApiError.RESOURCE_NOT_FOUND, null);

    }
}