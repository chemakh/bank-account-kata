package com.cl.bank.domain;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.Arrays;
import java.util.Objects;

public enum OperationType {

    WITHDRAWAL,
    DEPOSIT;

    @JsonCreator
    public static OperationType fromString(final String value) {
        return value != null ?
                Arrays.stream(values()).filter(val -> Objects.equals(val.toString().toUpperCase(), value.toUpperCase()))
                        .findFirst().orElse(WITHDRAWAL)
                : WITHDRAWAL;
    }

}
