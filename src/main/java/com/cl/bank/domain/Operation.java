package com.cl.bank.domain;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Optional;

@Entity
@Table(indexes = {@Index(name = "index_operation_reference", columnList = "reference", unique = true)})
@EntityListeners(AuditingEntityListener.class)
public class Operation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String reference;

    private OperationType type;

    @ManyToOne(optional = false)
    private Account account;

    private BigDecimal availableBalanceBeforeOperation;

    private BigDecimal availableBalanceAfterOperation;

    private BigDecimal amount;

    @Column(name = "created_date", updatable = false)
    @CreatedDate
    private LocalDateTime createdDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public OperationType getType() {
        return type;
    }

    public void setType(OperationType type) {
        this.type = type;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public BigDecimal getAvailableBalanceBeforeOperation() {
        return availableBalanceBeforeOperation;
    }

    public void setAvailableBalanceBeforeOperation(BigDecimal availableBalanceBeforeOperation) {
        this.availableBalanceBeforeOperation = availableBalanceBeforeOperation;
    }

    public BigDecimal getAvailableBalanceAfterOperation() {
        return availableBalanceAfterOperation;
    }

    public void setAvailableBalanceAfterOperation(BigDecimal availableBalanceAfterOperation) {
        this.availableBalanceAfterOperation = availableBalanceAfterOperation;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }


    @Override
    public boolean equals(Object object) {
        return Optional.ofNullable(object).filter(obj -> obj instanceof Operation).map(obj -> (Operation) obj).
                filter(ag -> getId() == null || Objects.equals(ag.getReference(), this.getReference())).
                filter(ag -> getId() != null || Objects.equals(ag, this)).
                isPresent();
    }

    @Override
    public int hashCode() {
        if (this.getReference() != null)
            return this.getReference().hashCode();
        else if (this.getId() != null)
            return this.getId().hashCode();
        else
            return super.hashCode();
    }
}
