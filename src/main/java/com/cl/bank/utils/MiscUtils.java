package com.cl.bank.utils;

import net.minidev.json.JSONObject;
import org.apache.commons.lang3.RandomStringUtils;

public class MiscUtils {

    public static JSONObject createSuccessfullyResult(String info) {
        JSONObject result = new JSONObject();
        result.put("result", "Operation Successfully Done");
        result.put("info", info);
        return result;
    }

    public static JSONObject createSuccessfullyResult() {
        JSONObject result = new JSONObject();
        result.put("result", "Operation Successfully Done");
        return result;
    }

    public static JSONObject createInProgressResult() {
        JSONObject result = new JSONObject();
        result.put("result", "Operation is in Progress ...");
        return result;
    }

    public static JSONObject createFailedResult(String msg) {
        JSONObject result = new JSONObject();
        result.put("result", "Operation Failed");
        result.put("info", msg);
        return result;
    }

    public static String generateReference() {
        return RandomStringUtils.randomAlphanumeric(15);
    }
}
