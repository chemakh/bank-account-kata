package com.cl.bank.utils;


import com.cl.bank.domain.Account;
import com.cl.bank.domain.Operation;
import com.cl.bank.repository.AccountRepository;
import com.cl.bank.repository.OperationRepository;
import com.cl.bank.web.dto.AccountDTO;
import com.cl.bank.web.dto.OperationDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring", imports = {Collectors.class, Arrays.class})
public abstract class ApiMapper {

    @Autowired
    protected AccountRepository accountRepository;


    @Autowired
    protected OperationRepository operationRepository;


    @Mappings({
            @Mapping(source = "account.reference", target = "accountReference")
    })
    public abstract OperationDTO fromBeanToDTO(Operation bean);

    @Mappings({
            @Mapping(target = "account",
                    expression = "java(accountRepository.findOneByReference(dto.getAccountReference()).orElse(null))"),
            @Mapping(target = "createdDate", ignore = true)
    })
    public abstract Operation fromDTOToBean(OperationDTO dto);

    @Mappings({
            @Mapping(source = "user.firstname", target = "firstname"),
            @Mapping(source = "user.lastname", target = "lastname"),
    })
    public abstract AccountDTO fromBeanToDTO(Account bean);

    @Mappings({
            @Mapping(target = "user", expression = "java(new User(dto.getFirstname(),dto.getLastname()))"),
            @Mapping(target = "createdDate", ignore = true),
            @Mapping(target = "modifiedDate", ignore = true)
    })
    public abstract Account fromDTOToBean(AccountDTO dto);

    @Mappings({
            @Mapping(target = "reference", ignore = true),
            @Mapping(target = "user", expression = "java(new User(dto.getFirstname(),dto.getLastname()))"),
            @Mapping(target = "availableBalance", ignore = true),
            @Mapping(target = "createdDate", ignore = true),
            @Mapping(target = "modifiedDate", ignore = true)
    })
    public abstract void updateBeanFromDto(AccountDTO dto, @MappingTarget Account bean);
}
