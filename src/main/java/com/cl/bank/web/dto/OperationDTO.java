package com.cl.bank.web.dto;

import com.cl.bank.domain.OperationType;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

public class OperationDTO {

    private String reference;

    private OperationType type;

    private String accountReference;

    private BigDecimal availableBalanceBeforeOperation;

    private BigDecimal availableBalanceAfterOperation;

    @NotNull
    private BigDecimal amount;

    private LocalDateTime createdDate;

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public OperationType getType() {
        return type;
    }

    public void setType(OperationType type) {
        this.type = type;
    }

    public String getAccountReference() {
        return accountReference;
    }

    public void setAccountReference(String accountReference) {
        this.accountReference = accountReference;
    }

    public BigDecimal getAvailableBalanceBeforeOperation() {
        return availableBalanceBeforeOperation;
    }

    public void setAvailableBalanceBeforeOperation(BigDecimal availableBalanceBeforeOperation) {
        this.availableBalanceBeforeOperation = availableBalanceBeforeOperation;
    }

    public BigDecimal getAvailableBalanceAfterOperation() {
        return availableBalanceAfterOperation;
    }

    public void setAvailableBalanceAfterOperation(BigDecimal availableBalanceAfterOperation) {
        this.availableBalanceAfterOperation = availableBalanceAfterOperation;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

}
