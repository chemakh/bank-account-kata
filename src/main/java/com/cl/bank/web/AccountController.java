package com.cl.bank.web;


import com.cl.bank.domain.Account;
import com.cl.bank.exception.BankApiException;
import com.cl.bank.service.AccountService;
import com.cl.bank.service.OperationService;
import com.cl.bank.web.dto.AccountDTO;
import com.cl.bank.web.dto.OperationDTO;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.minidev.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;

@RestController
@RequestMapping("/accounts")
public class AccountController {

    @Inject
    private AccountService accountService;

    @Inject
    private OperationService operationService;

    @RequestMapping(value = "",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Create Account Service")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation Executed Successfully", response = Account.class),
            @ApiResponse(code = 400, message = "Validation Error, Database conflict")
    })
    public AccountDTO createAccount(@RequestBody @Valid AccountDTO account) {

        return accountService.createAccount(account);
    }

    @RequestMapping(value = "",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Create Account Service")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation Executed Successfully", response = Account.class),
            @ApiResponse(code = 400, message = "Validation Error, Database conflict")
    })
    public AccountDTO updateAccount(@Valid @RequestBody AccountDTO account,
                                    @RequestParam(value = "accountReference") String accountReference) throws BankApiException {

        return accountService.updateAccount(account, accountReference);
    }


    @RequestMapping(value = "",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Get Account History Service")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation Executed Successfully", response = AccountDTO.class),
            @ApiResponse(code = 404, message = "Account with Ref not Found")
    })
    public JSONObject getAccount(@RequestParam(value = "accountReference") String accountReference) throws BankApiException {
        return accountService.getAccountHistory(accountReference);
    }

    @RequestMapping(value = "",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Delete Account Service")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation Executed Successfully", response = JSONObject.class),
            @ApiResponse(code = 404, message = "Account with Ref not Found")
    })
    public JSONObject deleteAccount(@RequestParam("accountReference") String accountReference) throws BankApiException {

        return accountService.deleteAccount(accountReference);
    }

    @RequestMapping(value = "operations",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Create Operation Service")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation Executed Successfully", response = Account.class),
            @ApiResponse(code = 400, message = "Validation Error, Database conflict")
    })
    public OperationDTO createOperation(@RequestBody @Valid OperationDTO operation,
                                        @RequestParam(value = "accountReference") String accountReference) throws BankApiException {

        return operationService.createOperation(operation,accountReference);
    }
}
