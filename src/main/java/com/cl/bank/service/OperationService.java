package com.cl.bank.service;

import com.cl.bank.domain.Account;
import com.cl.bank.domain.Operation;
import com.cl.bank.domain.OperationType;
import com.cl.bank.exception.BankApiException;
import com.cl.bank.utils.ApiMapper;
import com.cl.bank.repository.AccountRepository;
import com.cl.bank.repository.OperationRepository;
import com.cl.bank.utils.MiscUtils;
import com.cl.bank.web.dto.OperationDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;


@Service
public class OperationService {

    private final Logger logger = LoggerFactory.getLogger(OperationService.class);

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private OperationRepository operationRepository;

    @Autowired
    private ApiMapper mapper;

    @Transactional
    public OperationDTO createOperation(OperationDTO operationDTO, String accountReference) throws BankApiException {

        Account account = accountRepository.findOneByReference(accountReference)
                .orElseThrow(() -> BankApiException.resourceNotFoundExceptionBuilder("Account", accountReference));

        if (operationDTO.getAmount().compareTo(BigDecimal.ZERO) <= 0)
            throw BankApiException.unprocessableEntityExceptionBuilder("negative_amount", null);

        operationDTO.setReference(MiscUtils.generateReference());
        operationDTO.setAccountReference(accountReference);

        Operation operation = mapper.fromDTOToBean(operationDTO);
        operation.setAvailableBalanceBeforeOperation(account.getAvailableBalance());

        if(operation.getType() == OperationType.DEPOSIT)
            operation.setAvailableBalanceAfterOperation(operation.getAvailableBalanceBeforeOperation().add(operation.getAmount()));
        else {
            operation.setAvailableBalanceAfterOperation(operation.getAvailableBalanceBeforeOperation().subtract(operation.getAmount()));

            if(operation.getAvailableBalanceAfterOperation().compareTo(BigDecimal.ZERO) < 0)
                throw BankApiException.unprocessableEntityExceptionBuilder("enough_amount", null);

        }

        account.setAvailableBalance(operation.getAvailableBalanceAfterOperation());
        accountRepository.save(account);

        return mapper.fromBeanToDTO(operationRepository.save(operation));
    }
}
