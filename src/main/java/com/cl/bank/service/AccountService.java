package com.cl.bank.service;

import com.cl.bank.domain.Account;
import com.cl.bank.exception.BankApiException;
import com.cl.bank.utils.ApiMapper;
import com.cl.bank.repository.AccountRepository;
import com.cl.bank.repository.OperationRepository;
import com.cl.bank.utils.MiscUtils;
import com.cl.bank.web.dto.AccountDTO;
import net.minidev.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.stream.Collectors;

@Service
public class AccountService {

    private final Logger logger = LoggerFactory.getLogger(AccountService.class);

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private OperationRepository operationRepository;

    @Autowired
    private ApiMapper mapper;


    public AccountDTO createAccount(AccountDTO accountDTO) {

        accountDTO.setReference(MiscUtils.generateReference());
        accountDTO.setAvailableBalance(BigDecimal.ZERO);


        Account account = mapper.fromDTOToBean(accountDTO);

        return mapper.fromBeanToDTO(accountRepository.save(account));
    }

    public AccountDTO updateAccount(AccountDTO accountDTO, String accountReference) throws BankApiException {


        Account account = accountRepository.findOneByReference(accountReference)
                .orElseThrow(() -> BankApiException.resourceNotFoundExceptionBuilder("Account", accountReference));

        mapper.updateBeanFromDto(accountDTO, account);

        account = accountRepository.save(account);

        return mapper.fromBeanToDTO(account);

    }

    public JSONObject getAccountHistory(String accountReference) throws BankApiException {

        Account account = accountRepository.findOneByReference(accountReference)
                .orElseThrow(() -> BankApiException.resourceNotFoundExceptionBuilder("Account", accountReference));

        JSONObject result = new JSONObject();


        result.put("operation-history", operationRepository.findByAccountReferenceOrderByCreatedDateDesc(accountReference)
                .stream().map(mapper::fromBeanToDTO).collect(Collectors.toList()));
        result.put("account", mapper.fromBeanToDTO(account));

        return result;

    }

    public JSONObject deleteAccount(String accountReference) throws BankApiException {


        accountRepository.findOneByReference(accountReference)
                .orElseThrow(() -> BankApiException.resourceNotFoundExceptionBuilder("Account", accountReference));

        operationRepository.deleteByAccountReference(accountReference);
        accountRepository.deleteByReference(accountReference);
        return MiscUtils.createSuccessfullyResult("Account with Ref :" + accountReference + " successfully deleted");


    }

}
