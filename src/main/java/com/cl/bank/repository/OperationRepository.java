package com.cl.bank.repository;

import com.cl.bank.domain.Operation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface OperationRepository extends JpaRepository<Operation, Long> {

    Optional<Operation> findOneByReference(String operationReference);

    Optional<Operation> findOneByReferenceAndAccountReference(String operationReference, String accountReference);

    List<Operation> findByAccountReferenceOrderByCreatedDateDesc(String accountReference);

    @Transactional
    long deleteByAccountReference(String accountReference);

}
