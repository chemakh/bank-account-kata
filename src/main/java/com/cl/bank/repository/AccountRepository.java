package com.cl.bank.repository;

import com.cl.bank.domain.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Long> {

    Optional<Account> findOneByReference(String accountReference);

    @Transactional
    long deleteByReference(String accountReference);
}

